CREATE DATABASE dbo;
USE dbo;

CREATE TABLE GameCategory(        
GameCategoryId INT NOT NULL PRIMARY KEY,
Name VARCHAR(50)
);

INSERT 	INTO GameCategory (GameCategoryId, Name) 
VALUES ('1', 'wc3');
INSERT 	INTO GameCategory (GameCategoryId, Name) 
VALUES ('2', 'sc2');

CREATE TABLE Game(         
GameId INT NOT NULL PRIMARY KEY,
Name VARCHAR(50),
ReleaseDate DATE,
GameCategoryId INT,
Played INT(1),
FOREIGN KEY (GameCategoryId) REFERENCES GameCategory(GameCategoryId)
);


INSERT 	INTO Game (GameId, Name, ReleaseDate, GameCategoryId, Played) 
VALUES ('1', 'wc3', '2013-7-1', '1', '200');

CREATE TABLE Gender(         
GenderId INT NOT NULL PRIMARY KEY,
Description VARCHAR(20)
);

INSERT 	INTO Gender (GenderId, Description) 
VALUES ('1', 'RTS');

CREATE TABLE Player(         
PlayerId INT NOT NULL PRIMARY KEY,
Name VARCHAR(50),
GenderId INT,
FOREIGN KEY(GenderId) REFERENCES Gender(GenderId)
);


INSERT 	INTO Player (PlayerId, Name, GenderId) 
VALUES ('1', 'wc3', '1');

CREATE TABLE PlayerScore(         
PlayerScoreId INT NOT NULL PRIMARY KEY,
PlayerId INT,
GameId INT,
Score BIGINT,
ScoreData DATE,
FOREIGN KEY(PlayerId) REFERENCES Player(PlayerId),
FOREIGN KEY(GameId) REFERENCES Game(GameId)
);

INSERT 	INTO PlayerScore (PlayerScoreId, PlayerId, GameId, Score, ScoreData) 
VALUES ('1', '1', '1', '1', '2003-07-1');
